This is a set of simple monitoring scripts that run queries to the CMS HTCondor global pool, then plot results via google charts scripts in HTML files

/queries: query shell scripts, plus a few python ones (calculate averages, parse gWMS FE xml, etc)

/make_html: scripts that build the html files that present the plots with the accumulated data

This monitor is being migrated from:
http://submit-3.t2.ucsd.edu/CSstoragePath/aperez/HTML/

and https://github.com/aperezca/CMS-global-pool-monitor
